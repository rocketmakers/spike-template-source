# orbit-example-template-repo

Example repository for managing git templates through Orbit notification services. 

## Including paths to template files

In the root of your repository you need to include a json file, named after the third party service you are using for notifications, that provides your template metadata. This metadata includes: 
- paths to template layout files 
- any partials that need to be registered
- template names within a particular layout

[An example can be found here](./sendgrid.json) for registering/setting up sendgrid templates. The schema for these json files exists in the `@rocketmakers/orbit-template-http-repository` lib folder.

## Template directories

Within `./layouts/` create a directory named for your template. This should include the following files:

### Template file 
Template file with .handlebars file extension e.g. `my-cool-template.handlebars`. Within this file  the code should follow `Handlebars.js` syntax, if you're not familiar you can find the [docs here](https://handlebarsjs.com/). An example is shown below: 

```handlebars
<html>
  <head>
    <title>Newly added template</title>
  </head>
  <body>
    {{!-- Example of partial --}}
    {{> header}}
    {{!-- Example of writing & using custom helper (see run-scripts/valid-html.ts) --}}
    <p>To: {{name}}</p>
    {{> footer}}
  </body>
</html>
```

### model.ts
A `model.ts` file that includes:
- an `IModel` interface describing the data needed for your template. 
- an array of sample data of type `IModel` to be used in template validation scripts
- an annotation of the `IModel` interface to help generate a json schema using the sample data

```typescript
/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  name: string;
  username: string;
  requestDateTime: string;
}

export const sampleData: IModel[] = [
  {
    name: 'tim',
    username: 'tim@rocketmakers.com',
    requestDateTime: 'Sometime in the early 90s',
  },
];

```

### payloadSchema.json

After installing `@rocketmakers/orbit-template-http-repository` as a dependency, payload schemas can be generated for all layouts within this directory by running:

```bash
npx generate-schemas
```

This will generate a payload schema definition file within your layout template directory.