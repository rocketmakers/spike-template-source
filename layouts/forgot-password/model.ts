/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  name: string;
  username: string;
  requestDateTime: string;
}

export const sampleData: IModel[] = [
  {
    name: 'shea',
    username: 'shea@rocketmakers.com',
    requestDateTime: 'Sometime in the early 90s',
  },
  {
    name: 'keith',
    username: 'keith@rocketmakers.com',
    requestDateTime: 'Sometime in the early 90s',
  },
  {
    name: 'nick',
    username: 'nick@rocketmakers.com',
    requestDateTime: 'Sometime in the early 90s',
  },
];
