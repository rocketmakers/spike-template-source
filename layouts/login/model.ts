/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  name: string;
  username: string;
  deviceId: string;
  loginDateTime: string;
}

export const sampleData: IModel[] = [
  {
    name: 'shea',
    username: 'shea@rocketmakers.com',
    loginDateTime: 'Sometime in the early 90s',
    deviceId: '123ABC12',
  },
  {
    name: 'keith',
    username: 'keith@rocketmakers.com',
    loginDateTime: 'Sometime in the early 90s',
    deviceId: '123ABC12',
  },
  {
    name: 'nick',
    username: 'nick@rocketmakers.com',
    loginDateTime: 'Sometime in the early 90s',
    deviceId: '123ABC12',
  },
];
